// -*- C++ -*-
/* gtkglextmm - C++ Wrapper for GtkGLExt
 * Copyright (C) 2002-2003  Naofumi Yasufuku
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.
 */

#ifndef _GTKGLMM_H
#define _GTKGLMM_H

/** @mainpage gtkglextmm Reference Manual
 *
 * @section description Description
 *
 * GtkGLExt is an OpenGL extension to <a href="http://www.gtk.org/">GTK+</a>.
 * It provides additional GDK objects which support OpenGL rendering in GTK+ and
 * GtkWidget API add-ons to make GTK+ widgets OpenGL-capable.
 *
 * For instance, see Gtk::GL::Widget, Gtk::GL::DrawingArea and Gdk::GL::Window.
 *
 * See also the <a href="examples.html">Examples</a> page.
 *
 * @section basics Basic Usage
 *
 * Include the gtkglextmm header:
 * @code
 * #include <gtkglmm.h>
 * @endcode
 *
 * If your source file is @c program.cc, you can compile it with:
 * @code
 * g++ program.cc -o program `pkg-config --cflags --libs gtkglextmm-1.2`
 * @endcode
 *
 * Alternatively, if using autoconf, use the following in @c configure.ac:
 * @code
 * PKG_CHECK_MODULES([GTKGLEXTMM], [gtkglextmm-1.2])
 * @endcode
 * Then use the generated @c GTKGLEXTMM_CFLAGS and @c GTKGLEXTMM_LIBS variables
 * in the project @c Makefile.am files. For example:
 * @code
 * program_CPPFLAGS = $(GTKGLEXTMM_CFLAGS)
 * program_LDADD = $(GTKGLEXTMM_LIBS)
 * @endcode
 */

#include <gdkglmm.h>

#include <gtkmm/gl/defs.h>
#include <gtkmm/gl/version.h>
#include <gtkmm/gl/init.h>
#include <gtkmm/gl/widget.h>
#include <gtkmm/gl/drawingarea.h>

#endif // _GTKGLMM_H
