// -*- C++ -*-
/* gdkglextmm - C++ Wrapper for GdkGLExt
 * Copyright (C) 2002-2003  Naofumi Yasufuku
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.
 */

#include <gdk/gdkglenumtypes.h>
#include <gdk/gdkglconfig.h>

namespace Gdk
{
  namespace GL
  {

    Config::Config(const int* attrib_list)
      : Glib::Object(reinterpret_cast<GObject*>(
          gdk_gl_config_new(attrib_list)))
    {}

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
    Config::Config(const Glib::RefPtr<const Gdk::Screen>& screen,
                   const int* attrib_list)
      : Glib::Object(reinterpret_cast<GObject*>(
          gdk_gl_config_new_for_screen(const_cast<GdkScreen*>(Glib::unwrap<Gdk::Screen>(screen)),
                                       attrib_list)))
    {}
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

    Config::Config(ConfigMode mode)
      : Glib::Object(reinterpret_cast<GObject*>(
          gdk_gl_config_new_by_mode((GdkGLConfigMode)(mode))))
    {}

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
    Config::Config(const Glib::RefPtr<const Gdk::Screen>& screen,
                   ConfigMode mode)
      : Glib::Object(reinterpret_cast<GObject*>(
          gdk_gl_config_new_by_mode_for_screen(const_cast<GdkScreen*>(Glib::unwrap<Gdk::Screen>(screen)),
                                               (GdkGLConfigMode)(mode))))
    {}
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

    Glib::RefPtr<Config> Config::create(const int* attrib_list)
    {
      return Glib::wrap(gdk_gl_config_new(attrib_list));
    }

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
    Glib::RefPtr<Config> Config::create(const Glib::RefPtr<const Gdk::Screen>& screen,
                                        const int* attrib_list)
    {
      return Glib::wrap(
        gdk_gl_config_new_for_screen(const_cast<GdkScreen*>(Glib::unwrap<Gdk::Screen>(screen)),
                                     attrib_list));
    }
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

    Glib::RefPtr<Config> Config::create(ConfigMode mode)
    {
      return Glib::wrap(gdk_gl_config_new_by_mode((GdkGLConfigMode)(mode)));
    }

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
    Glib::RefPtr<Config> Config::create(const Glib::RefPtr<const Gdk::Screen>& screen,
                                        ConfigMode mode)
    {
      return Glib::wrap(
        gdk_gl_config_new_by_mode_for_screen(const_cast<GdkScreen*>(Glib::unwrap<Gdk::Screen>(screen)),
                                             (GdkGLConfigMode)(mode)));
    }
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
    Glib::RefPtr<Gdk::Screen> Config::get_screen()
    {
      Glib::RefPtr<Gdk::Screen> retvalue =
        Glib::wrap(gdk_gl_config_get_screen(gobj()));

      if (retvalue)
        retvalue->reference(); //The function does not do a ref for us.

      return retvalue;
    }
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
    Glib::RefPtr<const Gdk::Screen> Config::get_screen() const
    {
      Glib::RefPtr<const Gdk::Screen> retvalue =
        Glib::wrap(gdk_gl_config_get_screen(const_cast<GdkGLConfig*>(gobj())));

      if (retvalue)
        retvalue->reference(); //The function does not do a ref for us.

      return retvalue;
    }
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

  } // namespace GL
} // namespace Gdk
